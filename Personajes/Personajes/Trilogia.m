//
//  Trilogia.m
//  Personajes
//
//  Created by S209e19 on 28/08/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "Trilogia.h"

@implementation Trilogia

- (instancetype)initWithWeight:(NSUInteger)weight age:(NSUInteger)age{
    
    if (self = [super init] ) {
        
        _age = age;
        _weight = weight;
        
    }
    
    return self;
}

+ (NSString *)giveMeMyTrilogyWithMyWeight:(NSUInteger)weight age:(NSUInteger)age{
    
    NSString *response;
    
    NSMutableDictionary *mutableWeight = [NSMutableDictionary dictionary];
    
    [mutableWeight setObject:@"Estas Flaco, pero fuerte" forKey:@"40"];
    [mutableWeight setObject:@"Estas Fuerte" forKey:@"50"];
    [mutableWeight setObject:@"Buen peso, buena salud" forKey:@"60"];
    [mutableWeight setObject:@"Si es musculos estas grande, sino el ejercicio quiere ser tu amigo." forKey:@"70"];
    
    
    NSDictionary *data = @{
                           @"edad":@{@"10":@"Eres joven",
                                     @"20":@"Disfruta la vida",
                                     @"40":@"Eres Veterano",
                                     @"60":@"Lleno de sabiduría",
                                     @"80":@"Eres una leyenda. Te admiro"
                                     },
                           @"peso": mutableWeight.copy
                           
                           };
    
    NSString *msgAge;
    NSString *msgWeigth;
    
    if (age > 0 && age < 10) {
        msgAge = data[@"edad"][@"10"];
        
    } else if (age > 11 && age <= 20) {
        msgAge = data[@"edad"][@"20"];
        
    } else if (age > 21 && age <= 40) {
        msgAge = data[@"edad"][@"40"];
        
    } else if (age > 41 && age <= 60) {
        msgAge = data[@"edad"][@"60"];
        
    } else if (age > 61) {
        msgAge = data[@"edad"][@"80"];
        
    } else {
        msgAge = @"No se que decirte con tu edad";
    }
    
    if (weight > 0 && weight <= 40) {
        msgWeigth = data[@"peso"][@"40"];
        
    } else if (weight > 41 && weight <= 60) {
        msgWeigth = data[@"peso"][@"60"];
        
    } else if (weight > 61 ) {
        msgWeigth = data[@"peso"][@"70"];
        
    } else {
        
        response = @"No se que decirte con tu peso";
    }

    return response = [msgAge stringByAppendingString:[NSString stringWithFormat:@". %@", msgWeigth]];
   
    // otra forma
    // return response = [NSString stringWithFormat:@"%@. %@", msgAge, msgWeigth];
   
}


@end
