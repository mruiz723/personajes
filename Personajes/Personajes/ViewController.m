//
//  ViewController.m
//  Personajes
//
//  Created by S209e19 on 28/08/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "ViewController.h"
#import "Trilogia.h"

@interface ViewController ()

@end

@implementation ViewController{
    
    Trilogia * trilogia;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)chekit:(id)sender {
    
    trilogia = [[Trilogia alloc]initWithWeight:[[self.weight text]integerValue] age:self.age.text.integerValue];
    
    
    self.response.text = [Trilogia giveMeMyTrilogyWithMyWeight:[[self.weight text]integerValue] age:self.age.text.integerValue];
    
    [self.response setHidden:NO];
    
    [self.view endEditing:YES]; 
}
@end

















